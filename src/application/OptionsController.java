package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;

public class OptionsController {

	@FXML
	private Slider difficulteIA;
	@FXML
	private CheckBox afficherAvancementIA;
	
	private File fichierConfig;
	
	public static double[] FACILE = {128, 0.5, 2}; //h, lr, l
	public static double[] MOYEN = {512, 0.7, 4}; //h, lr, l
	public static double[] DIFFICILE = {2048, 0.9, 8}; //h, lr, l
	
	public void initialiser() throws FileNotFoundException {
		
		difficulteIA.setLabelFormatter(new StringConverter<Double>() { //Indiquer les niveaux de difficult� sur le slider
            @Override
            public String toString(Double n) {
                if (n == 0) 
                	return "Facile";
                if (n == 1) 
                	return "Moyen";
                if (n == 2) 
                	return "Difficile";
                return null;
            }

            @Override
            public Double fromString(String s) {
                switch (s) {
                    case "Facile":
                        return 0d;
                    case "Moyen":
                        return 1d;
                    case "Difficile":
                        return 2d;
                    default:
                        return null;
                }
            }
        });

		Tooltip tooltipIA = new Tooltip("L'IA apprend en permanence tant que l'application est ouverte");
		tooltipIA.setShowDelay(Duration.seconds(0.2));
		afficherAvancementIA.setTooltip(tooltipIA);
		
		fichierConfig = new File("data/config.txt");
		
		Scanner scanner = new Scanner(fichierConfig);
		if(scanner.hasNextLine()) { //Cette ligne contient la difficult� de l'IA
			difficulteIA.setValue(Integer.parseInt(scanner.nextLine()));
		}
		
		if(scanner.hasNextLine()) { //Cette ligne contient l'affichage de l'avancement de l'apprentissage
			afficherAvancementIA.setSelected(Boolean.parseBoolean(scanner.nextLine()));
		}
		scanner.close();
	}
	
	@FXML
	public void revenirAuMenuPrincipal(ActionEvent event) throws IOException {

        saveConfig();
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Accueil.fxml"));
        Parent root = loader.load();
        
        AccueilController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(); //On v�rifie si une sauvegarde existe (pour r�activer le bouton)
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	private void saveConfig() throws IOException {

		FileWriter fw = new FileWriter(fichierConfig);
		fw.write((int)difficulteIA.getValue() + "\n" + afficherAvancementIA.isSelected()); //On sauvegarde la difficult� de l'IA et l'affichage de la progression de l'apprentissage
		fw.close();
	}
}
