package application;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class MorpionJCJController extends MorpionController {

	@Override
	public void initialiser() {
		
		super.initialiser();
		textTour.setText("Au tour du joueur " + (model.getSymbole() == X ? "1" : "2"));
	}
	
	@Override
	public void jouerSymbole(MouseEvent event) {
		
		Node node = event.getPickResult().getIntersectedNode();
		Integer colonne = GridPane.getColumnIndex(node);
		Integer ligne = GridPane.getRowIndex(node);
		if(ligne == null)
			return;
		if(colonne == null)
			return;
		int index = ligne * 3 + colonne; //Repr�sente la case
		int labelIndex = index + 9; //Repr�sente le symbole sur la case
		if(model.getSymbole(index) == VIDE) { //On ne peut pas cliquer 2 fois sur la m�me case
			Label label = (Label)zoneDeJeu.getChildren().get(labelIndex);
			label.setText(model.getSymbole() == X ? "X" : "O");
			label.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
			model.setSymbole(model.getSymbole(), index);
			if(model.getVictoire() != 0) {
				zoneDeJeu.setOnMouseClicked(null); //On emp�che de cliquer sur la zone de jeu quand la partie est finie
				FadeTransition ft = new FadeTransition(Duration.millis(1000), topVBox); //Disparition de la VBox en haut (symboleTour et textTour)
				ft.setFromValue(1);
				ft.setToValue(0);
				ft.play();
				ft.setOnFinished(e -> { //Lorsque le texte a disparu, on le change et on le r�affiche avec une transition
					switch(model.getVictoire()) {
					case 1:
						textTour.setText("Le joueur 1 a gagn�");
						textTour.setTextFill(Color.BLUE);
						symboleTour.setTextFill(Color.BLUE);
						break;
					case 2:
						textTour.setText("Le joueur 2 a gagn�");
						textTour.setTextFill(Color.RED);
						symboleTour.setTextFill(Color.RED);
						break;
					case 3:
						textTour.setText("�galit�");
						textTour.setTextFill(Color.ORANGE);
						symboleTour.setText("");
						break;
					}
					FadeTransition ft2 = new FadeTransition(Duration.millis(1000), topVBox);
					ft2.setFromValue(0);
					ft2.setToValue(1);
					ft2.play();
				});
				ArrayList<Integer> symbolesGagnants = model.getSymbolesGagnants();
				for(Integer symboleGagnant : symbolesGagnants) {
					Label labelGagnant = (Label)zoneDeJeu.getChildren().get(symboleGagnant + 9);
					RotateTransition rt = new RotateTransition(Duration.millis(4000), labelGagnant); //Rotation des 3 symboles ayant permis la victoire
					rt.setAxis(new Point3D(0, 1, 0));
					rt.setByAngle(360);
					rt.setCycleCount(Timeline.INDEFINITE);
					rt.play();
				}
				FadeTransition ft3 = new FadeTransition(Duration.millis(1000), bottomVBox); //Apparaition de la VBox en bas (avec les boutons)
				ft3.setFromValue(0);
				ft3.setToValue(1);
				ft3.play();
				bottomVBox.setVisible(true);
				model.supprimerSauvegarde(); //On supprime la sauvegarde (car la partie est finie)
			}
			else {
				model.setSymbole(model.getSymbole() == X ? O : X); //On change le symbole
				textTour.setText("Au tour du joueur " + (model.getSymbole() == X ? "1" : "2"));
				textTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
				symboleTour.setText(model.getSymbole() == X ? "X" : "O");
				symboleTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
				model.sauvegarder(toString()); //On sauvegarder apr�s chaque coup (pour pouvoir quitter � tout moment et reprendre plus tard)
			}
		}
	}
	
	@Override
	public void charger() throws FileNotFoundException {
		
		model = new Model(); //On r�cup�re le model (qui contient toutes les donn�es de la partie en cours)
		model.charger();
		
		for(int i = 0; i < model.getSymboles().size(); i++) { //On modifie le plateau de jeu
			
			if(model.getSymboles().get(i) != 0) {
				Label label = (Label)zoneDeJeu.getChildren().get(i + 9);
				label.setText(model.getSymboles().get(i) == X ? "X" : "O");
				label.setTextFill(model.getSymboles().get(i) == X ? Color.BLUE : Color.RED);
			}
		}
		
		textTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
		symboleTour.setText(model.getSymbole() == X ? "X" : "O");
		symboleTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
		textTour.setText("Au tour du joueur " + (model.getSymbole() == X ? "1" : "2"));
	}
	
	@Override
	public String toString() {
		
		return 0 + "\n" + super.toString();
	}
}