package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AccueilController {
	
	@FXML
	private Button boutonReprendrePartie;
	@FXML
	private ProgressIndicator progressionIA;
	@FXML
	private VBox IAVBox;
	@FXML
	private Label labelProgressionIA;

	@FXML
	public void nouvellePartie(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("ChoixPartie.fxml"));
        Parent root = loader.load();
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	@FXML
	public void chargerPartie(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
		
		File fichierSauvegarde = new File("data/sauvegarde.txt");
		
		int modeDeJeu = 0;
		Parent root;
		Scanner scanner = new Scanner(fichierSauvegarde);
		if(scanner.hasNextLine()) { //Cette ligne contient le mode de jeu
			modeDeJeu = Integer.parseInt(scanner.nextLine());
		}
		scanner.close();
		
		if(modeDeJeu == 0) { //Mode de jeu JCJ
	        loader.setLocation(getClass().getResource("MorpionJCJ.fxml"));
	        root = loader.load();
	        
	        MorpionJCJController controller = loader.getController(); //On r�cup�re le contr�leur
	        controller.charger(); //On charge la sauvegarde
		}
		else { //Mode de jeu JCIA
			loader.setLocation(getClass().getResource("MorpionJCIA.fxml"));
	        root = loader.load();
	        
	        File fichierConfig = new File("data/config.txt");
	        scanner = new Scanner(fichierConfig);
	        int difficulte = 1;
			if(scanner.hasNextLine()) { //Cette ligne contient la difficult�
				difficulte = Integer.parseInt(scanner.nextLine());
			}
			scanner.close();
	        
	        MorpionJCIAController controller = loader.getController(); //On r�cup�re le contr�leur
	        controller.charger(difficulte); //On charge la sauvegarde
		}
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	@FXML
	public void options(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Options.fxml"));
        Parent root = loader.load();
        
        OptionsController controller = loader.getController();
        controller.initialiser(); //On initialise la sc�ne (checkbox selected, position slider)
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)

	}
	
	public void verifierSauvegarde() {
		
		File fichierSauvegarde = new File("data/sauvegarde.txt");
		if(fichierSauvegarde.exists())
			boutonReprendrePartie.setDisable(false); //Si une sauvegarde est disponible, on active le bouton pour reprendre la partie
	}
	
	public void initialiser() {
		
		verifierSauvegarde();
		File fichierConfig = new File("data/config.txt");
		
		Scanner scanner;
		int difficulte = 0;
		boolean afficherProgression = false;
		try {
			scanner = new Scanner(fichierConfig);
			if(scanner.hasNextLine()) { //Cette ligne contient la difficult� de l'IA
				difficulte = Integer.parseInt(scanner.nextLine());
			}
			if(scanner.hasNextLine()) { //Cette ligne contient l'affichage de la progression
				afficherProgression = Boolean.parseBoolean(scanner.nextLine());
				IAVBox.setVisible(afficherProgression);
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		switch(difficulte) { //On affiche la progression de l'IA pour la difficult� selectionn�e et on modifie le texte affich� suivant la difficult�e choisie
			case 0:
				progressionIA.progressProperty().bind(Main.progressPourcentageFacile);
				labelProgressionIA.setText("Progression de l'IA facile");
				break;
			case 1:
				progressionIA.progressProperty().bind(Main.progressPourcentageMoyen);
				labelProgressionIA.setText("Progression de l'IA moyen");
				break;
			case 2:
				progressionIA.progressProperty().bind(Main.progressPourcentageDifficile);
				labelProgressionIA.setText("Progression de l'IA difficile");
				break;
		}
	}
}
