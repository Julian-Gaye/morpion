package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Model {
	
	private ArrayList<Integer> symboles; //0 = vide, 1 = croix, 2 = rond
	private int victoire; //0 = partie en cours, 1 = victoire croix, 2 = victoire rond, 3 = �galit�
	private ArrayList<Integer> symbolesGagnants; //Liste des symboles gagnants
	private Integer symbole; //0 = vide, 1 = croix, 2 = rond
	private File fichierSauvegarde;
	
	public Model() {
		
		fichierSauvegarde = new File("data/sauvegarde.txt");
		symbolesGagnants = new ArrayList<>(3); //Le tableau des symboles gagnants contient 3 symboles
		victoire = 0;
		symboles = new ArrayList<Integer>(9);
		symboles.add(0); //On remplit le tableau de cases vides
		symboles.add(0);
		symboles.add(0);
		symboles.add(0);
		symboles.add(0);
		symboles.add(0);
		symboles.add(0);
		symboles.add(0);
		symboles.add(0);
	}

	public int getSymbole(int index) {
		
		return symboles.get(index);
	}

	public void setSymbole(Integer symbole, int index) {
		
		symboles.set(index, symbole);
		
		Boolean egalite = true;
		for(int s : symboles) {
			
			if(s == 0) { //Si au moins une case est vide, il n'y a pas �galit�
				egalite = false;
				break;
			}
		}
		
		
		if(symbole == symboles.get(3 * (index / 3)) && symbole == symboles.get(3 * (index / 3) + 1) && symbole == symboles.get(3 * (index / 3) + 2)) { //Une ligne est compl�te
			symbolesGagnants.add(3 * (index / 3));
			symbolesGagnants.add(3 * (index / 3) + 1);
			symbolesGagnants.add(3 * (index / 3) + 2);
			victoire = symbole;
		}
		else if(symbole == symboles.get(index % 3) && symbole == symboles.get(index % 3 + 3) && symbole == symboles.get(index % 3 + 6)) { //Une colonne est compl�te
			symbolesGagnants.add(index % 3);
			symbolesGagnants.add(index % 3 + 3);
			symbolesGagnants.add(index % 3 + 6);
			victoire = symbole;
		}
		else if(symbole == symboles.get(0) && symbole == symboles.get(4) && symbole == symboles.get(8)) { //Une diagonale est compl�te
			symbolesGagnants.add(0);
			symbolesGagnants.add(4);
			symbolesGagnants.add(8);
			victoire = symbole;
		}
		else if(symbole == symboles.get(2) && symbole == symboles.get(4) && symbole == symboles.get(6)) { //L'autre diagonale est compl�te
			symbolesGagnants.add(2);
			symbolesGagnants.add(4);
			symbolesGagnants.add(6);
			victoire = symbole;
		}
		else if(egalite) { //On v�rifie l'�galit� � la fin car on peut avoir toutes les cases remplies et une victoire d'un joueur
			victoire = 3;
		}
	}

	public int getVictoire() {
		
		return victoire;
	}
	
	public ArrayList<Integer> getSymbolesGagnants() {
		
		return symbolesGagnants;
	}
	
	public ArrayList<Integer> getSymboles() {
		
		return symboles;
	}
	
	public void setSymboles(ArrayList<Integer> symboles) {
		
		this.symboles = symboles;
	}
	
	public void setSymbole(int symbole) {
		
		this.symbole = symbole;
	}
	
	public int getSymbole() {
		
		return symbole;
	}

	/* Le fichier de sauvegarde contient 3 ligne
	 * ligne 1 : 0 ou 1 (JCJ ou JCIA)
	 * ligne 2 : 0 ou 1 (croix ou rond qui doit jouer)
	 * ligne 3 : liste des symboles sur la zone de jeu (0 = vide, 1 = croix, 2 = rond)
	 */
	public void sauvegarder(String s) {
		
		try {
			FileWriter fw = new FileWriter(fichierSauvegarde);
			fw.write(s);
			fw.close();
		} catch (IOException e) {
			System.err.println("Erreur lors de la sauvegarde");
			e.printStackTrace();
		}
	}
	
	public void charger() throws FileNotFoundException {
		
		fichierSauvegarde = new File("data/sauvegarde.txt");
		
		Scanner scanner = new Scanner(fichierSauvegarde);
		if(scanner.hasNextLine()) { //Cette ligne contient le mode de jeu (on la passe)
			scanner.nextLine();
		}
		
		if(scanner.hasNextLine()) { //Cette ligne contient le prochain joueur qui doit jouer
			symbole = Integer.parseInt(scanner.nextLine());
		}
		
		ArrayList<Integer> symboles = new ArrayList<>();
		if(scanner.hasNextLine()) { //Cette ligne contient l'ensemble des symboles d�j� plac�s
			String ligne = scanner.nextLine();
			for (int i = 0; i < ligne.length(); i++) {
			    char c = ligne.charAt(i);        
			    symboles.add(Character.getNumericValue(c));
			}
		}
		scanner.close();
		
		this.symboles = symboles; //On modifie le tableau dans le mod�le
	}
	
	public void supprimerSauvegarde() {
		
		fichierSauvegarde.delete();
	}
}
