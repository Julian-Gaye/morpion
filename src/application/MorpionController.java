package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public abstract class MorpionController {
	
	@FXML
	protected VBox bottomVBox;
	@FXML
	protected GridPane zoneDeJeu;
	@FXML 
	protected Label symboleTour;
	@FXML 
	protected Label textTour;
	@FXML
	protected VBox topVBox;
	
	protected Model model;
	
	public static int VIDE = 0;
	public static int X = 1;
	public static int O = 2;

	public void initialiser() {

		model = new Model(); //On r�cup�re le model (qui contient toutes les donn�es de la partie en cours)
		Random random = new Random();
		model.setSymbole(random.nextInt(2)); //On choisit al�atoirement le premier joueur
		if(model.getSymbole() != X)
			model.setSymbole(O);
		textTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
		symboleTour.setText(model.getSymbole() == X ? "X" : "O");
		symboleTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
	}
	
	@FXML
	public void revenirAuMenuPrincipal(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Accueil.fxml"));
        Parent root = loader.load();
        
        AccueilController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(); //On v�rifie si une sauvegarde existe (pour r�activer le bouton)
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	@FXML
	public abstract void jouerSymbole(MouseEvent event);
	
	@FXML
	public void jouerContreUnJoueur(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("MorpionJCJ.fxml"));
        Parent root = loader.load();
        
        MorpionJCJController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(); //On initialise la sc�ne
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	@FXML
	public void jouerContreUneIA(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("MorpionJCIA.fxml"));
        Parent root = loader.load();
        
        File fichierConfig = new File("data/config.txt");
        Scanner scanner = new Scanner(fichierConfig);
        int difficulte = 1;
		if(scanner.hasNextLine()) { //Cette ligne contient la difficult�
			difficulte = Integer.parseInt(scanner.nextLine());
		}
		scanner.close();
        
        MorpionJCIAController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(difficulte); //On initialise la sc�ne
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	public void charger() throws FileNotFoundException {

		model = new Model(); //On r�cup�re le model (qui contient toutes les donn�es de la partie en cours)
		model.charger();
		
		for(int i = 0; i < model.getSymboles().size(); i++) { //On modifie le plateau de jeu
			
			if(model.getSymboles().get(i) != 0) {
				Label label = (Label)zoneDeJeu.getChildren().get(i + 9);
				label.setText(model.getSymboles().get(i) == X ? "X" : "O");
				label.setTextFill(model.getSymboles().get(i) == X ? Color.BLUE : Color.RED);
			}
		}
		
		textTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
		symboleTour.setText(model.getSymbole() == X ? "X" : "O");
		symboleTour.setTextFill(model.getSymbole() == X ? Color.BLUE : Color.RED);
	}
	
	@Override
	public String toString() {
		
		String symboles = "";
		for(Integer i : model.getSymboles())
			symboles += i;
		return model.getSymbole() + "\n" + symboles;
	}
}
