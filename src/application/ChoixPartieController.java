package application;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Stage;

public class ChoixPartieController {

	@FXML
	public void lancerPartieJoueurContreJoueur(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("MorpionJCJ.fxml"));
        Parent root = loader.load();
        
        MorpionJCJController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(); //On initialise la sc�ne (premier joueur, modification des textes affich�s, etc.)
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	@FXML
	public void lancerPartieJoueurContreIA(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("MorpionJCIA.fxml"));
        Parent root = loader.load();
        
        File fichierConfig = new File("data/config.txt");
        Scanner scanner = new Scanner(fichierConfig);
        int difficulte = 1;
		if(scanner.hasNextLine()) { //Cette ligne contient la difficult�
			difficulte = Integer.parseInt(scanner.nextLine());
		}
		scanner.close();
        
        MorpionJCIAController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(difficulte); //On initialise la sc�ne (premier joueur, modification des textes affich�s, difficult� de l'IA etc.)
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
	
	@FXML
	public void revenirAccueil(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Accueil.fxml"));
        Parent root = loader.load();
        
        AccueilController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(); ///On initialise la sc�ne (activation bouton reprise, progress indicator, etc.)
        
		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //On r�cup�re le stage (fen�tre enti�re)
		stage.getScene().setRoot(root); //On change ce qui est affich� dans la sc�ne (on ne recr�e pas de sc�ne pour garder la m�me taille de fen�tre)
	}
}
