package application;
	
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import ia.Test;
import javafx.application.Application;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.concurrent.Task;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;

public class Main extends Application {
	
	private static ArrayList<Thread> threads;
	
	public static FloatProperty progressPourcentageFacile;
	public static FloatProperty progressPourcentageMoyen;
	public static FloatProperty progressPourcentageDifficile;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		progressPourcentageFacile = new SimpleFloatProperty();
		progressPourcentageMoyen = new SimpleFloatProperty();
		progressPourcentageDifficile = new SimpleFloatProperty();
		train(); //On train l'IA d�s qu'on lance l'application
		primaryStage.setOnCloseRequest(e -> {

			//Quand on ferme l'application (appui sur la croix), on arr�te l'apprentissage de l'IA
			for(Thread t : threads) {
				t.stop();
			}
		});
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Accueil.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root); //On cr�er la sc�ne
        
        File fichierConfig = new File("data/config.txt");
        if(!fichierConfig.exists()) {
        	
        	FileWriter fw = new FileWriter(fichierConfig);
			fw.write("0\nfalse"); //Par d�faut la difficult� est 0 et on n'affiche pas l'avancement de l'IA
			fw.close();
        }
        
        AccueilController controller = loader.getController(); //On r�cup�re le contr�leur
        controller.initialiser(); //On v�rifie si une sauvegarde existe (pour r�activer le bouton)
        	
		primaryStage.setMaximized(true); //On maximise la fen�tre (fen�tr� plein �cran)
		primaryStage.setScene(scene); //On met la sc�ne dans le stage
		primaryStage.setTitle("Tic-Tac-Toe"); //On met un titre � la fen�tre
		primaryStage.show(); //On affiche le stage
	}
	
	public static void train() {
		
        //On train les 3 difficult�s en parall�le
        int iterationsMaxApprentissage = 1000000000; //On limite le nombre d'it�rations de l'algorithme d'apprentissage (pour avoir un pourcentage pour le progress indicator et car �a ne sert � rien de surentra�ner l'IA)
        
        //On cr�er une nouvelle task pour chaque difficult�
        Task<Void> taskFacile = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				Test test = new Test();
				test.nombreIterations.addListener((ov, oldValue, newValue) -> {
		        	
					progressPourcentageFacile.set((float)newValue.intValue()/iterationsMaxApprentissage);
				});
				test.train((int)OptionsController.FACILE[0], OptionsController.FACILE[1], (int)OptionsController.FACILE[2], iterationsMaxApprentissage);
				return null;
			}
        	
        };
        Task<Void> taskMoyen = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				Test test = new Test();
				test.nombreIterations.addListener((ov, oldValue, newValue) -> {
		        	
					progressPourcentageMoyen.set((float)newValue.intValue()/iterationsMaxApprentissage);
				});
				test.train((int)OptionsController.MOYEN[0], OptionsController.MOYEN[1], (int)OptionsController.MOYEN[2], iterationsMaxApprentissage);
				return null;
			}
        	
        };
        Task<Void> taskDifficile = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				Test test = new Test();
				test.nombreIterations.addListener((ov, oldValue, newValue) -> {
			        
					progressPourcentageDifficile.set((float)newValue.intValue()/iterationsMaxApprentissage);
				});
				
				test.train((int)OptionsController.DIFFICILE[0], OptionsController.DIFFICILE[1], (int)OptionsController.DIFFICILE[2], iterationsMaxApprentissage);
				return null;
			}
        	
        };
        
        //On lance chaque task dans un thread diff�rent (pour pouvoir train 3 difficult� en m�me temps et sans figer l'�cran)
        Thread tFacile = new Thread(taskFacile);
        Thread tMoyen = new Thread(taskMoyen);
        Thread tDifficile = new Thread(taskDifficile);
        
        tFacile.start();
        tMoyen.start();
        tDifficile.start();
        
        //On garde les threads pour pouvoir les arr�ter quand on ferme l'application
        threads = new ArrayList<Thread>();
        
        threads.add(tFacile);
        threads.add(tMoyen);
        threads.add(tDifficile);
    }
	
	public static int play(int difficulte, ArrayList<Integer> symboles) {
		
		//On fait jouer l'IA correspondant � la difficult� selectionn�e
		Test test = new Test();
		switch(difficulte) {
			case 0: 
				return test.play((int)OptionsController.FACILE[0], OptionsController.FACILE[1], (int)OptionsController.FACILE[2], symboles);
			case 1: 
				return test.play((int)OptionsController.MOYEN[0], OptionsController.MOYEN[1], (int)OptionsController.MOYEN[2], symboles);
			case 2: 
				return test.play((int)OptionsController.DIFFICILE[0], OptionsController.DIFFICILE[1], (int)OptionsController.DIFFICILE[2], symboles);
			default:
				return 0;
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
